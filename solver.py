    #
    # 1. get rows of numbers, using 0 for blank
    # 2. covert rows to columns and boxes
    # 3. evaluate each box and replace 0 with array of possible values
    # 4. compare each box and cell to corresponding row and column to eliminate values.
    #
    #
    # apply other algorithms to solve.
    #
    # Brute force solve
    # Once a single value remains, remove the cell array and replace with single value
    # Check suduko_box cells for any remaining arrays or 0's. if none, then puzzle is complete
    #

import numpy as nmp


###################################################### FUNCTIONS ######################################################
def is_row_clean(row):
    if len(row) < 9:
        response = "row is too short"
    elif len(row) > 9:
        response = "row is too long"
    else:
        response = ""
    return response

def make_column(grid):
    # transpose results in column format
    col = grid.transpose()
    return col

def make_box(grid):
    #arrange results in box format
    a = nmp.array(grid[0:3])
    top_box = nmp.array(
        [[a[..., 0], a[..., 1], a[..., 2]], [a[..., 3], a[..., 4], a[..., 5]], [a[..., 6], a[..., 7], a[..., 8]]])
    a = nmp.array(grid[3:6])
    middle_box = nmp.array(
        [[a[..., 0], a[..., 1], a[..., 2]], [a[..., 3], a[..., 4], a[..., 5]], [a[..., 6], a[..., 7], a[..., 8]]])
    box = nmp.array([top_box,middle_box])
    a = nmp.array(grid[6:9])
    bottom_box = nmp.array(
        [[a[..., 0], a[..., 1], a[..., 2]], [a[..., 3], a[..., 4], a[..., 5]], [a[..., 6], a[..., 7], a[..., 8]]])
    box = nmp.array([top_box,middle_box,bottom_box])
    return box

def box_get_all_possible(grid):
    # this replaces all the 0 with array of possible matches
    for l0 in range(0,3):
        for l1 in range(0,3):               # each square
            min = "1|2|3|4|5|6|7|8|9|"
            for l2 in range(0,3):
                for l3 in range(0,3):
                    min = min.replace(str(grid[l0][l1][l2][l3]) + "|", '')
            org = nmp.array(grid[l0][l1])
            org[org == "0"] = min
            grid[l0][l1]=org

    return grid


def match_row_col(boxs,rows,cols):
    # eliminates box variables against corresponding rows and columns
    g = 0
    for l0 in range(0,3):                    # each collection of 3 across in the box grid
        b = 0
        #print("\n")
        #print("Group: " + str(g))
        for l1 in range(0,3):
            #print("Box: " + str(b))
            for l2 in range(0,3):
                r = 0
                #print("Next Col")
                for l3 in range(0,3):
                    if len(boxs[l0][l1][l2][l3]) > 1:
                        #print(boxs[l0][l1][l2][l3])
                        #print("Row: " + str(g+r) + " Value: " + str(boxs[l0][l1][l2][l3]))
                        for val in rows[g+r]:
                            new = boxs[l0][l1][l2][l3].replace(str(val) + "|", '')     #this does not replace the actual value. need to know index of array.
                            boxs[l0][l1][l2][l3] = new
                        #
                        #print("Row: " + str(g + r) + " Value: " + str(boxs[l0][l1][l2][l3]))
                    r += 1
                #print(rr + r)

            b += 1
        g += 3
    return boxs

########################################### START APP ###########################################################
print("welcome to my Suduko solver")
print("SUduko is solved in rows and colums")
print("you will be prompted to enter each row (1-9)")
answer = input("Ready? (Y) ")
if answer.lower() == "n":
    print("Goodbye")
    exit()

suduko_row = nmp.array([[6, 3, 0, 2, 0, 8, 0, 1, 0],[2, 0, 0, 0, 5, 0, 0, 8, 9],[1, 0, 9, 0, 6, 0, 0, 3, 0],
                        [0, 0, 8, 0, 0, 6, 0, 5, 0],[0, 0, 0, 1, 8, 7, 0, 0, 0],[0, 6, 0, 5, 0, 0, 9, 0, 0],
                        [0, 9, 0, 0, 7, 0, 1, 0, 6],[8, 1, 0, 0, 2, 0, 0, 0, 5],[0, 2, 0, 4, 0, 3, 0, 9, 7]])


# suduko_row = nmp.empty((0, 9))
# row_count = 1
# while row_count <= 6:
#     value = input("Input Row " + str(row_count) + " Array as 1,0,6, etc: ")
#     value = value.split(",")
#     # print(value)
#     is_clean = is_row_clean(value)
#
#     if is_clean != "":
#         print(is_clean)
#     else:
#         suduko_row = nmp.append(suduko_row, nmp.array([value]), axis=0)
#         row_count += 1


#suduko_row = suduko_row.astype(nmp.str)
#print(suduko_row)

# create col and box arrays
suduko_col = make_column(suduko_row)
suduko_box = make_box(suduko_row)

#suduko_col = suduko_col.astype(nmp.str)



#in box array replace 0 with all possible matches.
#box will hold the solution
suduko_box = suduko_box.astype(nmp.str)
suduko_box = box_get_all_possible(suduko_box)
#

match_row_col(suduko_box,suduko_row,suduko_col)
print(suduko_box)

'''
print(suduko_row)
print("\n")
print(suduko_col)
print("\n")
print(suduko_box)
'''

